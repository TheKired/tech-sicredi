import { DragonListDataObj } from "../api/dragon/DataInterfaces";

const DragonSorter = {
  sortAlphabetically: function(list: DragonListDataObj): DragonListDataObj {
    return list.sort((a, b) => a.name > b.name ? 1 : -1);
  }
}

export default DragonSorter;
