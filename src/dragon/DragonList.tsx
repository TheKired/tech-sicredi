import React, { Component } from "react";
import { Link } from "react-router-dom";
import { DragonListDataObj } from "../api/dragon/DataInterfaces";
import DragonListData from "../api/dragon/DragonListData";
import "./DragonList.scss";
import DragonSorter from "./DragonSorter";

type State = {
  dragonList?: DragonListDataObj;
  loading: boolean;
};

class DragonList extends Component<{}, State> {
  private dragonListData?: DragonListData;
  constructor(props: {}) {
    super(props);

    this.state = { loading: true, dragonList: undefined };
  }

  async componentDidMount() {
    this.dragonListData = new DragonListData();
    const list = await this.dragonListData.get();
    const sortedList = this.sortDragons(list);
    this.setState({
      loading: false,
      dragonList: sortedList
    })
  }

  sortDragons(dragonList: DragonListDataObj): DragonListDataObj {
    return DragonSorter.sortAlphabetically(dragonList);
  }

  render() {
    return (
      <div className="dragon-list">
        <h2>All Dragons</h2>
        <ul>
          <li><Link className="create-dragon-button" to={"/dragon/new"} >Create new</Link></li>
          {this.state.dragonList?.map(({id, name}) => {
            return ( 
            <li>
              <Link to={`/dragon/${id}`} >
                <div className="dragon-name">{name}</div>
                <div className="dragon-id">
                  <div className="va-outter">
                    <div className="va-inner">
                    {id}
                    </div>
                  </div>
                </div>
              </Link>
            </li>
          )     
          })}
        </ul>
      </div>
    );
  }
}

export default DragonList;
