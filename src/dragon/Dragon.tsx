import React, { ChangeEvent, Component } from "react";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { DragonDataObj, EmptyDragon } from "../api/dragon/DataInterfaces";
import DragonData from "../api/dragon/DragonData";
import TextInput from "../components/TextInput";
import "./Dragon.scss";

enum ComponentModes {
  Edit = "edit",
  Display = "display",
}

type Props = RouteComponentProps<{ id: string }>;

type State = {
  dragon?: DragonDataObj;
  loading: boolean;
  mode: ComponentModes;
};

const initialState = {
  dragon: EmptyDragon,
  loading: true,
  mode: ComponentModes.Display,
};

class Dragon extends Component<Props, State> {
  private id?: string = undefined;
  private dragonData?: DragonData;
  constructor(props: any) {
    super(props);
    this.state = initialState;
    this.id = props.match.params.id;
  }

  componentDidMount(): void {
    this.handleComponentMode();
    this.initializeData();
  }

  handleComponentMode(): void {
    const path = this.props.location.pathname;
    let mode: ComponentModes;
    if (path.includes("new") || path.includes("edit")) {
      mode = ComponentModes.Edit;
    } else {
      mode = ComponentModes.Display;
    }
    this.setState({ mode });
  }

  async initializeData() {
    this.dragonData = new DragonData(this.id);
    if (this.id) {
      await this.fetchAndSetDragon();
    }
    this.setLoading(false);
  }

  async fetchAndSetDragon(): Promise<void> {
    const dragonDataObj = await this.dragonData!.get();
    this.updateDragon(dragonDataObj);
  }

  getTextDisplayFor(propertyName: keyof DragonDataObj): JSX.Element {
    return <div className="dragon-text" id={propertyName}>{this.state.dragon?.[propertyName]}</div>;
  }

  getTextInputFor(propertyName: keyof DragonDataObj): JSX.Element {
    return (
      <TextInput
        value={this.state.dragon?.[propertyName]}
        onChange={this.onChange}
        required
        name={propertyName}
        id={propertyName}
        disabled={this.state.loading}
        className="dragon-input"
      />
    );
  }

  onChange = (event: ChangeEvent<HTMLInputElement>): void => {
    const value = event.currentTarget.value;
    const name = event.currentTarget.name;
    this.setState({ dragon: { ...this.state.dragon!, [name]: value } });
  };

  onEdit = (): void => {
    this.switchComponentModeTo(ComponentModes.Edit);
  };

  onCancel = (): void => {
    this.fetchAndSetDragon();
    this.switchComponentModeTo(ComponentModes.Display);
  };

  onSave = async (): Promise<void> => {
    this.setLoading(true);
    let updatedDragon: DragonDataObj;
    if (this.id) {
      updatedDragon = await this.editDragon();
    } else {
      updatedDragon = await this.newDragon();
    }
    this.updateDragon(updatedDragon);
    this.switchComponentModeTo(ComponentModes.Display);
    this.setLoading(false)
  };

  onDelete = async (): Promise<void> => {
    this.setLoading(true);
    await this.deleteDragon();
    this.props.history.push(`/dragon`);
  };

  async editDragon(): Promise<DragonDataObj> {
    return this.dragonData!.edit(this.state.dragon!);
  }
  
  async newDragon(): Promise<DragonDataObj> {
    return this.dragonData!.new(this.state.dragon!);
  }

  async deleteDragon(): Promise<DragonDataObj> {
    return this.dragonData!.delete();
  }

  updateDragon(dragon: DragonDataObj): void {
    this.setState({ dragon });
    this.updateDragonId(dragon.id);
  }

  updateDragonId(id: string) {
    this.id = id;
    this.dragonData?.setId(id);
  }

  setLoading(loading: boolean): void {
    this.setState({ loading });
  }

  switchComponentModeTo(mode: ComponentModes): void {
    this.setState({ mode });
    if (mode === ComponentModes.Display) {
      this.goToPathIfNotAlready(`/dragon/${this.id}`);
    } else if (mode === ComponentModes.Edit) {
      this.goToPathIfNotAlready(`/dragon/${this.id}/edit`);
    }
  }

  goToPathIfNotAlready(path: string) {
    if (this.props.history.location.pathname !== path) {
      this.props.history.push(path);
    };
  }

  modeMap = {
    display: {
      elementClass: "dragon-display",
      titleText: () => `Viewing dragon ${this.state.dragon?.id}`,
      textComponent: this.getTextDisplayFor.bind(this),
      buttons: () => (
        <div className="buttons">
          <button onClick={this.onEdit}>Edit</button>
        </div>
      ),
    },
    edit: {
      elementClass: "dragon-edit",
      titleText: () => `Editing ${this.state.dragon?.id ? "dragon " + this.state.dragon.id : "new dragon"}`,
      textComponent: this.getTextInputFor.bind(this),
      buttons: () => (
        <div className="buttons">
          <button onClick={this.onCancel} disabled={!this.state?.dragon?.id}>
            Cancel
          </button>
          <button onClick={this.onSave}>Save</button>
          <button onClick={this.onDelete} disabled={!this.state?.dragon?.id}>
            Delete
          </button>
        </div>
      ),
    },
  };

  render() {
    const map = this.modeMap[this.state.mode];
    return (
      <div className={map.elementClass + " dragon"}>
        <h2>{map.titleText()}</h2>
        <div className="field-line">
          <label htmlFor="name">Name:</label>
          {map.textComponent("name")}
        </div>
        <div className="field-line">
          <label htmlFor="type">Type:</label>
          {map.textComponent("type")}
        </div>
        <div className="field-line">
          <label htmlFor="createdAt">Creation date:</label>
          {this.getTextDisplayFor("createdAt")}
        </div>
        {map.buttons()}
      </div>
    );
  }
}

export default withRouter(Dragon) as React.ComponentType<any>;
