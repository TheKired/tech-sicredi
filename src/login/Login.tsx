import React, { ChangeEvent, Component, FormEvent } from "react";
import { AuthResponse, UserAuth } from "../api/UserAuth";
import TextInput from "../components/TextInput";
import "./Login.scss";

type State = {
  user: string;
  password: string;
  isLoggingIn: boolean;
}

const initialState = {
  user: "",
  password: "",
  isLoggingIn: false
}

class Login extends Component<{}, State> {
  private auth: UserAuth;
  constructor(props: {}) {
    super(props);

    this.auth = new UserAuth();
    this.state = initialState;
  }

  onChange = (event: ChangeEvent<HTMLInputElement>) => {
    const value = event.currentTarget.value;
    const name = event.currentTarget.name;
    this.setState( { ...this.state, [name]: value } );
  }

  onSubmit = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    this.setIsLogginIn(true);
    const { user, password } = this.state;

    const response = await this.auth.login(user, password);
    this.handleAuthResponse(response);
  }
  
  handleAuthResponse(response: AuthResponse) {
    this.setIsLogginIn(false);

    if (response === AuthResponse.Yes) {
      window.location.reload();
      return;
    }

    // This'll do for now
    alert("User or password incorrect. tip: password is \"123\"");
  }

  setIsLogginIn(flag: boolean) {
    const isLoggingIn = flag;
    this.setState({isLoggingIn});
  }

  render() {
    return (
      <div className="Login">
        <img className="logo" src={require("../img/logo.png")} alt="Dragon logo"/>
        <h2 className="logo-text">Dragon app</h2>
        <form onSubmit={this.onSubmit} className="login-form">
          <div className="login-input-group">
            <TextInput onChange={this.onChange} required name="user" placeholder="User" className="login-input" />
            <TextInput onChange={this.onChange} required name="password" placeholder="Password" className="login-input" type="password" />
          </div>
        <button className="login-button" disabled={this.state.isLoggingIn} >{this.state.isLoggingIn ? "Logging in..." : "Login"}</button>
        </form>
      </div>
    );
  }
}

export default Login;
