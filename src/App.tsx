import React, { Component } from "react";
import "./App.scss";
import Routes from "./router/Routes";

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="app-content">
          <Routes />
        </div>
      </div>
    );
  }
}

export default App;
