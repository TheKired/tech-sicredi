import React, { AllHTMLAttributes, ChangeEvent, Component } from "react";

type Props = AllHTMLAttributes<HTMLInputElement> & {
  valuePromise?: Promise<string>;
}

type State = {
  value: string,
  loading: boolean
}

class TextInput extends Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.handleInitialState();
    this.handleChangeMethods();
  }

  handleInitialState() {
    const state = {
      value: "",
      loading: false
    };

    if (this.props.valuePromise) {
      this.setPromisedValue();
      state.loading = true;
    }
    
    // eslint-disable-next-line react/no-direct-mutation-state
    this.state = state;
  }

  async setPromisedValue() {
    const value = await this.props.valuePromise as string;
    const loading = false;
    this.setState({ value, loading })
  }

  handleChangeMethods() {
    const { onChange } = this.props;
    if (typeof onChange !== "function") {
      return;
    }

    this.change = (event: ChangeEvent<HTMLInputElement>) => {
      this.updateInput(event);
      onChange(event);
    }
  }

  updateInput = (event: ChangeEvent<HTMLInputElement>) => { 
    const value = event.currentTarget.value;
    this.setState({ value });
  }

  change = this.updateInput;

  render() {
    const { valuePromise, onChange, ...other } = this.props;
    return (
      <input type="text" value={this.state.value} onChange={this.change} disabled={this.state.loading} {...other} />
    );
  }
}

export default TextInput;
