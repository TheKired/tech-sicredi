const { Request } = require("./Request");

const requestProps = {
  get: { url: "http://somepath.com/get", method: "GET" },
  putWithHeaders: {
    url: "http://somepath.com/put",
    method: "PUT",
    headers: [{ key: "someKey", value: "someValue" }],
  },
  postWithBody: {
    url: "http://somepath.com/post",
    method: "POST",
    body: "some dragon content",
  },
};

const XMLHttpRequest = function () {};
XMLHttpRequest.prototype = {
  constructor: XMLHttpRequest,
  readyState: 0,
  status: 0,
  responseURL: "",
  open: function (method, url) {
    this.responseURL = url;
  },
  setRequestHeader: () => {},
  send: () => {},
  onreadystatechange: () => {},
};
XMLHttpRequest.DONE = 4;
global.XMLHttpRequest = XMLHttpRequest;

jest.spyOn(XMLHttpRequest.prototype, "open");
jest.spyOn(XMLHttpRequest.prototype, "setRequestHeader");
jest.spyOn(XMLHttpRequest.prototype, "send");

const Test = function () {
  this.request = new Request();
  this.callDo = (...args) => {
    this.returnedValue = this.request.do(...args);
  };
  this.callIsRequestDone = (...args) => {
    this.returnedValue = this.request.isRequestDone(...args);
  };
  this.callGetRequestUrl = (...args) => {
    this.returnedValue = this.request.getRequestUrl(...args);
  };
};
var unittest;

beforeEach(() => {
  jest.clearAllMocks();
  unittest = new Test();
});

describe("do()", () => {
  it("Should return a Promise", () => {
    unittest.callDo(requestProps.get);
    expect(unittest.returnedValue).toBeInstanceOf(Promise);
  });
  it("Should call 'open' function with correct method and url", () => {
    unittest.callDo(requestProps.get);
    expect(unittest.request.xhr.open).toHaveBeenCalledWith(
      requestProps.get.method,
      requestProps.get.url
    );
  });
  it("Should call 'setRequestHeader' function with correct arguments", () => {
    unittest.callDo(requestProps.putWithHeaders);
    expect(unittest.request.xhr.setRequestHeader).toHaveBeenCalledWith(
      requestProps.putWithHeaders.headers[0].key,
      requestProps.putWithHeaders.headers[0].value
    );
  });
  it("Should call 'send' function with body", () => {
    unittest.callDo(requestProps.postWithBody);
    expect(unittest.request.xhr.send).toHaveBeenCalledWith(
      requestProps.postWithBody.body
    );
  });
});
describe("isRequestDone()", () => {
  it("Should return false when request hasn't the DONE state code", () => {
    unittest.request.xhr.readyState = 3;
    unittest.callIsRequestDone();
    expect(unittest.returnedValue).toBe(false);
  });
  it("Should return true when request has the DONE state code", () => {
    unittest.request.xhr.readyState = 4;
    unittest.callIsRequestDone();
    expect(unittest.returnedValue).toBe(true);
  });
});
describe("getRequestUrl()", () => {
  it("Should return the request URL", () => {
    unittest.callDo(requestProps.get);
    unittest.callGetRequestUrl();
    expect(unittest.returnedValue).toBe(requestProps.get.url);
  });
});
describe("Promise resolution", () => {
  it("Should resolve promise when request done and successful", (done) => {
    unittest.callDo(requestProps.get);
    unittest.returnedValue.then((response) => {
      expect(response).toBe("ALL GOOD!");
      done();
    });
    unittest.request.xhr.response = "ALL GOOD!";
    unittest.request.xhr.readyState = 4;
    unittest.request.xhr.status = 200;
    unittest.request.xhr.onreadystatechange();
  });
  it("Should not resolve promise when request not done", (done) => {
    unittest.callDo(requestProps.get);
    unittest.returnedValue.then((response) => {
      throw new Error("Shouldn't resolve promise")
    });
    unittest.request.xhr.response = "WON'T GET HERE";
    unittest.request.xhr.readyState = 3;
    unittest.request.xhr.onreadystatechange();
    setTimeout(done, 100);
  });
  it("Should reject promise when status code is bad", (done) => {
    unittest.callDo(requestProps.get);
    unittest.returnedValue.then(() => {
      throw new Error("Shouldn't resolve promise")
    },() => {
      expect();
      done();
    });
    unittest.request.xhr.readyState = 4;
    unittest.request.xhr.status = 404;
    unittest.request.xhr.onreadystatechange();
  });
});
