export class BrowserStorage {
  private storage: Storage;
  constructor() {
    this.storage = localStorage;
  }
  public get(key: string): string | null {
    return this.storage.getItem(key);
  }
  public set(key: string, value: string): void {
    this.storage.setItem(key, value);
  }
}
