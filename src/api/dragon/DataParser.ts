import { DragonDataObj, DragonListDataObj } from "./DataInterfaces";

const DataParser = {
  parseDragonDates(data: DragonDataObj): DragonDataObj {
    const { createdAt } = data;
    const parsedDate = new Date(createdAt).toLocaleDateString();
    return { ...data, createdAt: parsedDate };
  },
  parseSingleDragon(data: string): DragonDataObj {
    const partiallyParsed = JSON.parse(data) as DragonDataObj;
    return DataParser.parseDragonDates(partiallyParsed);
  },
  parseMultipleDragons(data: string): DragonListDataObj {
    const partiallyParsed = JSON.parse(data) as DragonListDataObj;
    return partiallyParsed.map(DataParser.parseDragonDates);
  },
};

export default DataParser;
