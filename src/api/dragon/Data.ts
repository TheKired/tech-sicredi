import { ENDPOINT } from "../Constants";
import { Request, RequestProps } from "../Request";
import { PossibleDataObjs } from "./DataInterfaces";

enum DataRequestTypes {
  Fetch = "fetch",
  New = "new",
  Edit = "edit",
  Delete = "delete",
}

export abstract class Data<T extends PossibleDataObjs> {
  private requestPromise: Promise<string> | null;

  protected ENDPOINT = ENDPOINT;

  constructor(protected dataObj: T | null = null) {
    this.requestPromise = null;
  }

  protected abstract getFetchProps(): RequestProps;
  protected abstract getCreateProps(): RequestProps;
  protected abstract getUpdateProps(): RequestProps;
  protected abstract getDeleteProps(): RequestProps;

  private async fetch(): Promise<void> {this.clearData();
    this.clearData();
    this.requestPromise = this.makeRequest(DataRequestTypes.Fetch);
    this.requestPromise.then(response => {
      this.dataObj = this.parseData(response);
      this.requestPromise = null;
    })
  }

  private clearData(): void {
    this.dataObj = null;
  }

  public get(): Promise<T> {
    if (this.dataObj) {
      return new Promise((resolve) => {
        resolve(this.dataObj as T);
      });
    }
    if (!this.requestPromise) {
      this.fetch();
    }
    return new Promise((resolve, reject) => {
      this.requestPromise!.then((response) => {
        resolve(this.parseData(response));
      });
    });
  }

  public async new(bodyObj: T): Promise<T> {
    this.clearData();
    const response = await this.makeRequest(DataRequestTypes.New, bodyObj);
    return this.parseData(response);
  }
  
  public async edit(bodyObj: T): Promise<T> {
    this.clearData();
    const response = await this.makeRequest(DataRequestTypes.Edit, bodyObj);
    return this.parseData(response);
  }

  public async delete(): Promise<T> {
    this.clearData();
    const response = await this.makeRequest(DataRequestTypes.Delete);
    return this.parseData(response);
  }

  private async makeRequest(type: DataRequestTypes, bodyObj?: T): Promise<string> {
    const request = new Request();
    const props = this.requestTypeToFunctionMap[type]();
    const body = JSON.stringify(bodyObj);
    return request.do({ ...props, body });
  }

  private requestTypeToFunctionMap = {
    fetch: this.getFetchProps.bind(this),
    new: this.getCreateProps.bind(this),
    edit: this.getUpdateProps.bind(this),
    delete: this.getDeleteProps.bind(this),
  }

  protected abstract parseData(data: string): T;
}
