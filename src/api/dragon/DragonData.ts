import { RequestProps } from "../Request";
import { Data } from "./Data";
import { DragonDataObj } from "./DataInterfaces";
import DataParser from "./DataParser";

const jsonHeader = {
  key: "Content-Type",
  value: "application/json"
}

class DragonData extends Data<DragonDataObj> {
  private localPath = "/dragon";
  private url = `${this.ENDPOINT}${this.localPath}`;
  constructor(private id?: string) {
    super();
  }

  public setId(id: string): void {
    this.id = id;
  }

  protected getFetchProps(): RequestProps {
    return { method: "GET", url: `${this.url}/${this.id}` };
  }
  protected getCreateProps(): RequestProps {
    return { method: "POST", url: `${this.url}`, headers: [ jsonHeader ] };
  }
  protected getUpdateProps(): RequestProps {
    return { method: "PUT", url: `${this.url}/${this.id}`, headers: [ jsonHeader ] };
  }
  protected getDeleteProps(): RequestProps {
    return { method: "DELETE", url: `${this.url}/${this.id}` };
  }

  protected parseData(data: string): DragonDataObj {
    return DataParser.parseSingleDragon(data);
  }
}

export default DragonData;
