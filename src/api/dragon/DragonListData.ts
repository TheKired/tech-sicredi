import { RequestProps } from "../Request";
import { Data } from "./Data";
import { DragonListDataObj } from "./DataInterfaces";
import DataParser from "./DataParser";
import DragonData from "./DragonData";

class DragonListData extends Data<DragonListDataObj> {
  private localPath = "/dragon";
  private url = `${this.ENDPOINT}${this.localPath}`;
  private children: DragonData[] = [];

  protected getCreateProps(): RequestProps { throw new Error("Method not applicable") }
  protected getUpdateProps(): RequestProps { throw new Error("Method not applicable") }
  protected getDeleteProps(): RequestProps { throw new Error("Method not applicable") }
  protected getFetchProps(): RequestProps {
    return { method: "GET", url: this.url };
  }

  protected parseData(data: string): DragonListDataObj {
    return DataParser.parseMultipleDragons(data);
    // const parsedData = DataParser.parseMultipleDragons(data);
    // this.children = parsedData.map(dragonData => new DragonData(dragonData))
  }

}

export default DragonListData;