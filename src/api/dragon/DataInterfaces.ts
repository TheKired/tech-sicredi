type IndexedObject = { [index: string]: any };

export type PossibleDataObjs = DragonDataObj | DragonListDataObj;

export interface DragonDataObj {
  id: string;
  createdAt: string;
  name: string;
  type: string;
  histories: string[];
}

export const EmptyDragon = {
  id: "",
  createdAt: "",
  name: "",
  type: "",
  histories: []
}

export enum DragonDataDateProps {
  createdAt = "createdAt"
}

export type DragonListDataObj = DragonDataObj[];
