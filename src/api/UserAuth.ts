import { BrowserStorage } from "./BrowserStorage";

const CREDENTIAL_STORAGE = "CRED_STOR";
const CREDENTIAL_VALID_KEY = "VALID";
const CREDENTIAL_INVALID_KEY = "INVALID";

const USERNAME_STORAGE = "USER_STOR";

export enum AuthResponse {
  Yes = "YES",
  No = "NO"
}

export class UserAuth {
  private storage: BrowserStorage;

  constructor() {
    this.storage = new BrowserStorage();
  }

  public login(user: string, pass: string): Promise<AuthResponse> {
    const handle = (resolve: (a: AuthResponse) => void) => {
      if (pass !== "123") {
        return resolve(AuthResponse.No);
      }
      this.storage.set(CREDENTIAL_STORAGE, CREDENTIAL_VALID_KEY);
      this.storage.set(USERNAME_STORAGE, user);
      resolve(AuthResponse.Yes);
    }
    const promise = (resolve: (a: AuthResponse) => void) => {
      setTimeout(() => { handle(resolve) }, 1500);
    }
    return new Promise(promise);
  }
  
  public logout(): void {
    this.storage.set(CREDENTIAL_STORAGE, CREDENTIAL_INVALID_KEY);
    this.storage.set(USERNAME_STORAGE, "");
    window.location.reload(); // this will do for this test
  }

  public isLogged(): boolean {
    const key = this.storage.get(CREDENTIAL_STORAGE);
    if (!key) {
      return false;
    }
    return this.isKeyValid(key);
  }

  private isKeyValid(key: string): boolean {
    return key === CREDENTIAL_VALID_KEY;
  }
}

export default UserAuth;