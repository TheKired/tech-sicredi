type RequestMethod = "GET" | "POST" | "PUT" | "DELETE";

export interface RequestProps {
  url: string;
  method: RequestMethod;
  body?: any,
  headers?: { key: string, value: string }[]
}

export class Request {
  private xhr: XMLHttpRequest;

  constructor() {
    this.xhr = new XMLHttpRequest();
  }

  public do({ url, method, body = {}, headers = [] }: RequestProps): Promise<string> {
    const promise = new Promise(this.resolveOrRejectRequest.bind(this));
    this.xhr.open(method, url);
    headers.forEach((header) => this.xhr.setRequestHeader(header.key, header.value));
    this.xhr.send(body);
    return promise;
  }

  public isRequestDone(): boolean {
    return this.isRequestStateDone(this.xhr.readyState);
  }

  public getRequestUrl(): string {
    return this.xhr.responseURL;
  }

  private resolveOrRejectRequest(
    resolve: (response: string) => void,
    reject: () => void
  ): void {
    const that = this;
    this.xhr.onreadystatechange = function () {
      if (!that.isRequestStateDone(this.readyState)) {
        return;
      }
      if (that.isStatusSuccessful(this.status)) {
        resolve(this.response);
      }
      reject();
    };
  }

  private isRequestStateDone(state: number): boolean {
    return state === XMLHttpRequest.DONE;
  }

  private isStatusSuccessful(status: number): boolean {
    return status === 0 || (status >= 200 && status < 400);
  }
}
