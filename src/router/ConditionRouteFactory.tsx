import React, { ReactNode } from "react";
import {
  Redirect,
  Route,
  RouteComponentProps,
  RouteProps,
} from "react-router-dom";

class ConditionRouteFactory {
  private children: ReactNode;
  constructor(
    private condition: () => boolean,
    private fallbackRoute: string
  ) {}

  public create() {
    return this.createConditionRoute.bind(this);
  }

  private createConditionRoute({ children, ...props }: RouteProps): JSX.Element {
    this.persistContent(children);
    return this.routeThatRendersByCondition(props);
  };

  private persistContent(children: ReactNode) {
    this.children = children;
  }

  private routeThatRendersByCondition(props: RouteProps): JSX.Element {
    return <Route {...props} render={this.renderByCondition.bind(this)} />;
  }

  private renderByCondition({ location }: RouteComponentProps) {
    if (this.condition()) {
      return this.showContent();
    }
    return this.redirectToFallback(location);
  }

  private showContent() {
    return this.children;
  }

  private redirectToFallback(location: any): JSX.Element {
    return (
    <Redirect
      to={{
        pathname: this.fallbackRoute,
        state: { from: location },
      }}
    />
    )
  }
}

export function createRoute(condition: () => boolean, fallbackRoute: string) {
  return new ConditionRouteFactory(condition, fallbackRoute).create();
}

export default createRoute;
