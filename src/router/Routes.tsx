import React, { Component } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { UserAuth } from "../api/UserAuth";
import Dragon from "../dragon/Dragon";
import DragonList from "../dragon/DragonList";
import Login from "../login/Login";
import * as ConditionRouteFactory from "./ConditionRouteFactory";

const isLogged = () => new UserAuth().isLogged();
const PrivateRoute = ConditionRouteFactory.createRoute(isLogged, "/login");

const isNotLogged = () => !isLogged();
const LoginRoute = ConditionRouteFactory.createRoute(isNotLogged, "/");

class Routes extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <PrivateRoute path={["/", "/dragon"]} exact={true}>
            <DragonList />
          </PrivateRoute>
          <PrivateRoute
            path={["/dragon/new", "/dragon/:id", "/dragon/:id/edit"]}
            exact={true}
          >
            <Dragon />
          </PrivateRoute>

          <LoginRoute path="/login">
            <Login />
          </LoginRoute>

          <Route path={"/"}>
            <h1>404</h1>
          </Route>
        </Switch>
      </BrowserRouter>
    );
  }
}

export default Routes;
